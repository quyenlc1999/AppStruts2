<%--
  Created by IntelliJ IDEA.
  User: quyenlc2
  Date: 8/25/2022
  Time: 8:28 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
  <title>Contact Manager - Struts2 Hibernate Example</title>
</head>
<body>

<h1>Contact Manager</h1>
<s:actionerror/>

<s:form action="add" method="post">
  <s:textfield name="contact.firstName" label="Firstname"/>
  <s:textfield name="contact.lastName" label="Lastname"/>
  <s:textfield name="contact.emailId" label="Email"/>
  <s:textfield name="contact.cellNo" label="Cell No."/>
  <s:textfield name="contact.website" label="Homepage"/>
  <s:textfield name="contact.birthDate" label="Birthdate"/>
  <s:submit value="Add Contact" align="center"/>
</s:form>
</body>
</html>
