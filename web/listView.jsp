<%--
  Created by IntelliJ IDEA.
  User: quyenlc2
  Date: 8/26/2022
  Time: 11:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Danh sách liên hệ</title>
</head>
<body>
<s:a action="listView">Hiển thị dánh sách</s:a>
<h2>Danh sách liên hệ</h2>
<table>
    <tr style="border: 1px solid black;">
        <th>Tên</th>
        <th>Email</th>
        <th>Điện thoại</th>
        <th>Ngày sinh</th>
        <th>Trang chủ</th>
        <th>Cập nhật</th>
        <th>Xóa</th>
    </tr>
    <s:iterator value="contactList" var="contact">
        <tr>
            <td><s:property value="lastName"/>, <s:property value="firstName"/> </td>
            <td><s:property value="emailId"/></td>
            <td><s:property value="cellNo"/></td>
            <td><s:property value="birthDate"/></td>
            <td><a href="<s:property value="website"/>">link</a></td>
            <td><a href="updateView?id=<s:property value="id"/>">Cập nhật</a></td>
            <td><a href="delete?id=<s:property value="id"/>">Xóa</a></td>
        </tr>
    </s:iterator>
</table>
            <button><a href="viewFormAdd">Thêm mới</a></button>
</body>
</html>
