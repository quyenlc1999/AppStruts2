package controller;

import model.Contact;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ContactManagerController extends HibernateUtil {

    public Contact add(Contact contact){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(contact);
        session.getTransaction().commit();
        return contact;
    }

    public Contact delete(Long id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Contact contact = (Contact) session.load(Contact.class ,id);
        if(contact != null){
            session.delete(contact);
        }
        session.getTransaction().commit();
        return contact;
    }

    public List<Contact> list(){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<Contact> contacts = new ArrayList<Contact>();
        try{
            contacts = (List<Contact>) session.createQuery("from Contact").list();
        }catch (HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }
        session.getTransaction().commit();
        return contacts;
    }

}
