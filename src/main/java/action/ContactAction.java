package action;

import com.opensymphony.xwork2.ActionSupport;
import controller.ContactManagerController;
import model.Contact;

import java.util.List;


public class ContactAction extends ActionSupport {
    private Contact contact;
    private List<Contact> contactList;
    private Long id;
    private ContactManagerController contactManagerController;

    public String execute(){
//        if(contact != null){
//            contactManagerController.add(contact);
//        }
        ContactManagerController controller = new ContactManagerController();
        this.contactList = controller.list();
        return SUCCESS;
    }
    public String add(){
        try{
            ContactManagerController controller = new ContactManagerController();
            controller.add(getContact());
            this.contactList = controller.list();
        }catch (Exception e){
            e.printStackTrace();
        }
        return SUCCESS;
    }
    public String delete(){
        ContactManagerController controller = new ContactManagerController();
        controller.delete(id);
        return SUCCESS;
    }

    public String viewContactAdd(){
        return SUCCESS;
    }


    public String listView(){
        ContactManagerController controller = new ContactManagerController();
        this.contactList = controller.list();
        return SUCCESS;
    }
    public String viewFormAdd(){
        return SUCCESS;
    }



    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContactManagerController getContactManagerController() {
        return contactManagerController;
    }

    public void setContactManagerController(ContactManagerController contactManagerController) {
        this.contactManagerController = contactManagerController;
    }
}
